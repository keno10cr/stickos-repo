//Create Objects/Elements from IDS
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
var stickerRana = document.getElementById('rana1');
var prevBtn = document.getElementById('prevBtn');
var nextBtn = document.getElementById('nextBtn');
var es = document.getElementById('espanholTico');
var en = document.getElementById('englishGringo');


var eyeSet = document.getElementById('eyeSet');
var bocaSet = document.getElementById('bocaSet');
var dichoSet = document.getElementById('dichoSet');
var headSet = document.getElementById('headSet');
var radios = document.getElementsByName('facePart');
var stksmodal = document.getElementById('closeModal');
var stkssection = document.getElementById('StickersSection');

//These are the Listeners for the Objects/Elements.
if(document.getElementById("iSpeakTICO") !== null)//check if element exists
{
  prevBtn.addEventListener("click", prevFace);
  nextBtn.addEventListener("click", nextFace);

}

if(document.getElementById("myModal") !== null)//check if element exists
{
  stksmodal.addEventListener("click", showTop);
  stkssection.addEventListener("click", hideTop);
}

if(document.getElementById("aboutSection") !== null)//check if element exists
{
  es.addEventListener("click", esSitio);
  en.addEventListener("click", enSite);
}

//stickerRana.addEventListener("mouseover", myShake);
//eyeSet.addEventListener("mouseenter", blink);
//eyeSet.addEventListener("dblclick", inlove);
//eyeSet.addEventListener("mouseleave", defaultFace);


//Sets of Data via URL
//vvvvvvvvvvvvvvvvvvvvvvvvvvvv
//var imageUrl = "https://image.ibb.co";
var imageUrl = "images/titi";
var myEyes = {
  "position": 0,
  "urls": ["/mono-oj-centrados.png", "/mono-oj-cerrados.png", "/mono-oj-gafas.png", "/mono-oj-lentes.png", "/mono-oj-normales.png", "/mono-oj-panic.png", "/mono-oj-pepis.png", "/mono-oj-blanco.png", "/mono-oj-enojado.png", "/mono-oj-muerto.png", "/mono-oj-peladosarriba.png", "/mono-oj-ladod.png", "/mono-oj-ladoi.png", "/mono-oj-dormido.png", "/mono-oj-chill.png", "/mono-oj-winki.png", "/mono-oj-winkd.png"]
};

var myBocas = {
  "position": 0,
  "urls": ["/mono-bc-dientes.png", "/mono-bc-feliz.png", "/mono-bc-nhe.png", "/mono-bc-normal.png", "/mono-bc-o.png", "/mono-bc-triste.png", "/mono-bc-arrugada.png", "/mono-bc-triste1.png", "/mono-bc-beso.png", "/mono-bc-ladofeliz.png", "/mono-bc-rico.png", "/mono-bc-lenguaafuera.png", "/mono-seria-sm.png", "/mono-bc-enfermo.png", "/mono-bc-ooo.png", "/mono-bc-babas.png"]
};

var myHead = {
  "position": 0,
  "urls": ["/zz-mono-cbz-cejas-arriba.png", "/zz-mono-cbz-cejas-normal.png", "/zz-mono-cbz-cejas-bajo.png", "/zz-mono-cbz-cejas-izq.png", "/zz-mono-cbz-cejas-der.png", "/zz-mono-cbz-cejas-enojado.png"]
};

var myDichos = {
  "position": 0,
  "urls": ["/dicho-diay.png", "/dicho-diobvio.png", "/dicho-mae.png", "/dicho-puravida.png", "/dicho-tuanis.png", "/dicho-tonces.png", "/dicho-soytico.png", "/dicho-gracias.png", "/dicho-oiga.png", "/dicho-alchile.png", "/dicho-unasbirras.png", "/dicho-pongale.png", "/dicho-entodas.png", "/dicho-qtigra.png", "/dicho-unateja.png", "/dicho-querico.png", "/dicho-yo-reciclo.png", "/dicho-verde-organico.png", "/dicho-querico.png", "/dicho-un-rojo.png", "/dicho-puerto-viejo-recicla.png", "/dicho-plastico-azul.png", "/dicho-papel-carton-gris.png", "/dicho-cr-recicla.png", "/dicho-cahuita-recicla.png", "/dicho-bici.png", "/dicho-aluminio-amarillo.png"]
};

var SiteContent = {
  "intro": ["La idea de los <strong>stickos</strong> es promover Costa Rica con diseños únicos, divertidos e informativos. Estas <strong>calcomanías</strong> que puedes pegar donde sea son una forma excelente de mantener los recuerdos de Costa Rica muy presentes.<br/><br/>Las calcomanías no van a quitar espacio del equipaje, no son pesadas y son no perecederas además son resistentes y perfectas para llevarse un bonito recuerdo del paseo, además estan hechos de la más alta calidad, y así como los <strong>buenos recuerdos</strong> van a durar por siempre.", "The idea of the <strong>stickos</strong> is to promote Costa Rica with unique, fun and informative designs, these are a going to be a great way to keep your memories from Costa Rica alive with <strong>stickers</strong> that you can paste anywhere.<br/><br/>Stickers are a perfect souvenir when you travel because these won’t take space wont add weight to your luggage and they are non perishable, which is really important when you are traveling, besides they are made of really high-quality materials and just as a <strong>good memories</strong> I'm sure these will last forever."],
  "goals1": ['La educación es muy importante, <strong>la meta de este 2023 es que de cada sticker vendido "una teja" (cien colones) es para ayudar a centros educativos con la compra de materiales escolares, como lápices, cuadernos y libros.</strong> Este primer año va a ser una escuela en Puerto Viejo.', "Education is really important, <strong>the goal of this 2023 is to take 100 colones to help schools by getting them supplies just like pencils, notebooks and books.</strong> This first year we will pick a school from Puerto Viejo."],
  "goals2": ["La idea de cada diseño es traerte <strong>buenos recuerdos de las cosas que puedes hacer y escuchar en Costa Rica.</strong> También si está interesado en aprender español, estas expresiones le van a ayudar a entender una que otra palabra más fácil.", "The idea of ​​each design is to bring you <strong>good memories of the things you can find, do and hear in Costa Rica.</strong> Also if you are into learning Spanish I’m sure these expressions will help you pick up a few words and situations faster."],
  "goals3": ['<strong>Pura Vida</strong>, <strong>Mae</strong> y <strong>Tuanis</strong> son algunas de las expresiones que va a escuchar de los ticos. <strong>Crea tu propio sticko con todas <a href="#navEmoji">las posibles combinaciones.</a></strong>', '<strong>Pura Vida</strong>, <strong>Mae</strong> and <strong>Tuanis</strong> are just a few of the expressions you will hear from ticos, <strong>Come up with your own stickos, there are <a href="#navEmoji">thousands of combinations!</a></strong>'],
  "greentalkheader": ["Sostenibilidad", "Sustainability"],
  "greentalk1": ["Educación para el desarrollo sostenible", "Desde la abolición del ejército y la fundación de la segunda república de Costa Rica la educación ha sido un pilar fundamental para la idiosincrasia de los ticos. No obstante el principal problema del decremento de nuestro sistema educativo radica en la falta de creación de capacidades para generar proyectos de inversión en pro a mejorar la infraestructura, los insumos y materiales didácticos y la constante innovación del sistema en nuestro país. <strong>El Informe del Estado de La Nación 2018</strong> refleja que los principales problemas al día de hoy en nuestro país son la falta de libros, pocos espacios para promover la motora gruesa y problemas para cumplir con las rutinas diarias por falta de material didáctico. Las aulas presentan problemas importantes en falta de luz, ruido y temperaturas poco agradables. En Stickos creemos que es necesario mejorar la calidad educativa en todos los niveles para crear ciudadanos conscientes de su entorno, así como de los problemas sociales y ambientales con los que nos enfrentamos en nuestro día a día. Por esta razón, nuestra esperanza es la juventud, empezando por lo más importante que ha forjado nuestra sociedad: La Educación. ", "Education for sustainable development", "Since the abolition of the Costa Rican army and the founding of the second republic of Costa Rica, education has been a fundamental pillar for the idiosyncrasies of Costa Ricans. However, the main problem of the decrease of our educational system lies in the lack of capacity building to generate investment projects in order to improve infrastructure, supplies and didactic materials and the constant innovation of the system in our country. <strong>The State of the Nation Report 2018</strong> reflects that the main problems to date in our country are the lack of books, few spaces to promote gross motor and problems to meet daily routines for lack of teaching materials. The classrooms present important problems in lack of light, noise and unpleasant temperatures. Stickos believes that it is necessary to improve the quality of education at all levels to create citizens aware of their environment, as well as the social and environmental problems that we face in our day to day. For this reason, our hope is youth, starting with the most important thing that our society has forged: Education."],
  "greentalk2": ["Una teja crea futuro sostenible:", "Nuestra meta en Stickos es destinar “una teja” (cien colones) por cada sticker que obtengas en la compra de <strong>materiales escolares y material didáctico.</strong> El dinero será destinado a escuelas rurales ubicadas en las comunidades en donde nos encontramos. Llevamos las herramientas para que estos niños puedan cumplir sus sueños ¡se parte de este sueño tú también!", "“La teja” creates a sustainable future:", "Our goal in Stickos is to allocate “una teja” (one hundred colones) for each sticker that you obtain, in the purchase of <strong>school materials and didactic material.</strong> The money will be allocated to rural schools located in the communities where we are located. We carry the tools so that these children can fulfill their dreams, be part of this dream, you too!"],
  "greentalk3": ["¡Costa Rica Verde!", "Los ticos compartimos muchas cosas en común y valores imprescindibles que nos identifican: nuestro amor por la patria, la democracia, la paz y nuestro amor por la naturaleza. Como país nos hemos fijado metas importantes para combatir el calentamiento global y desde Stickos queremos aportar nuestro grano de arena a esta carrera contra el tiempo llamada <strong>Cambio Climático.</strong> En Stickos estamos conscientes que el Cambio Climático es el principal reto que tenemos como generación, como país y como planeta, es por esto que hemos direccionado nuestra estrategia como emprendedores en crear contenido eco-friendly a través de nuestros diseños con concientización ambiental en des carbonización, reciclaje, preservación del recurso hídrico y preservación de toda clase de vida marina y terrestre.", "Costa Rica Green!", "Us the “ticos” share many things in common and essential values that identify us: our love for the homeland, democracy, peace and our love for nature. As a country we have set important goals to combat global warming and in Stickos we want to contribute our grain of sand to this race against time called <strong>Climate Change.</strong> In Stickos we are aware that Climate Change is the main challenge we have as a generation, as a country and as a planet, that is why we have directed our strategy as entrepreneurs in creating eco-friendly content through our designs with environmental awareness in decarbonization , recycling, preservation of water resources and preservation of all kinds of marine and terrestrial life."],
  "Stickos": ["Estos son algunos de los <strong>stickos</strong> que puedes encontrar en diferentes puntos de Costa Rica.", "These are some of the <strong>stickos</strong> that you can find around Costa Rica."],
  "iSpeakTICO": ["Crea <strong>tus propios stickos</strong> con todas las <strong>expresiones disponibles.</strong>", "Create <strong>your own stickos</strong> with all the <strong>available expressions</strong>"],
  "contact": ["Gracias por el apoyo,<br/> cualquier duda por correo:<br/> <span>stickoscr@gmail.com</span> De fijo respondo!", "Thanks for the support,<br/> if anything please send me an email:<br/> <span>stickoscr@gmail.com</span> I'll do my best to reply!"],
  "emojis": ["Cejas", "Ojos", "Bocas", "Dichos", "Eyebrows", "Eyes", "Mouths", "Sayings"],
  "shop5": ["Uno de los mejores parques nacionales en Costa Rica.", "One of the best national parks in Costa Rica."],
  "shop1": ["Deliciosa comida caribeña. Ubicado en el corazón de Puerto Viejo.", "Delicious caribbean food. Located right in Puerto Viejo downtown."],
  "shop2": ["Perfecto para llevar y seguir conociendo el pueblo. Tiene que probar la pizza en cono! Buenísima!", "An unique dish in town, walk around and wonder this amazing place. You gotta try the cone pizza! Terrific!"],
  "shop3": ["Espacio de trabajo conjunto con una cafeteria vegana. Ubicado en el segundo piso con una vista diferente del pueblo.", "A coworking space with a vegan cafe. It's located on a second floor, with a wonderful view of the town."],
  "shop4": ["Excelente comida para todos los gustos y una hermosa playa galardonada bandera azul ecológica.", "Excelent food for everyone and with an ecological beach (bandera azul) and seaview rigth in front of you!"],
  "navmenus": ["Consiguelos", "Contacto", "Get Yours", "Contact"],
  "bannertext": ["Stickers de Costa Rica", "Stickers from Costa Rica", "Crea el tuyo", "Create yours"],
  "sitetitles": ["Sobre la idea", "Ayudar a Escuelas", "Diseños", "Crea el tuyo", "About the idea", "Help Schools", "Designs", "Create Yours"],
  "dichoForm": ["¿Ha escuchado algún otro dicho?", "Have you heard any other saying?", "Escuche esto en:", "I heard this at:", "Dicho", "Saying"]
};




/**
 * Strokes
 */
$(function(){
  $('.stroke-double, .stroke-single').attr('title', function(){
    return $(this).html();
  });
});

/**
 * Shake Elements
vvvvvvvvvvvvvvvvvvvvvvvvv
 */
function myShake() {
  //https://api.jqueryui.com/shake-effect/
  $("#rana1").effect("shake");
}



/*Functions For Emoji Creator
vvvvvvvvvvvvvvvvvvvvvvvvvvv
*/

function changeFace() {
  //works along with Radio Bottom
  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      //alert ("about to change the "+ radios[i].value);
      if (radios[i].value === "eyes") { //eyes changes here:
        var nPositions = myEyes.urls.length - 1;

        do {
          var xNumber = Math.floor((Math.random() * nPositions) + 0);
        }
        while (xNumber === myEyes.position);
        eyeSet.src = imageUrl + myEyes.urls[xNumber];
        myEyes.position = xNumber;

        break;
      } else if (radios[i].value === "boca") { //boca changes here:
        var nPositions = myBocas.urls.length - 1;

        do {
          var xNumber = Math.floor((Math.random() * nPositions) + 0);
        }
        while (xNumber === myBocas.position);
        bocaSet.src = imageUrl + myBocas.urls[xNumber];
        myBocas.position = xNumber;

        break;
      } else if (radios[i].value === "dicho") { //boca changes here:
        var nPositions = myDichos.urls.length - 1;

        do {
          var xNumber = Math.floor((Math.random() * nPositions) + 0);
        }
        while (xNumber === myDichos.position);
        dichoSet.src = imageUrl + myDichos.urls[xNumber];
        myDichos.position = xNumber;

        break;
      } else if (radios[i].value === "cejas") { //cejas changes here:
        var nPositions = myHead.urls.length - 1;

        do {
          var xNumber = Math.floor((Math.random() * nPositions) + 0);
        }
        while (xNumber === myHead.position);
        headSet.src = imageUrl + myHead.urls[xNumber];
        myHead.position = xNumber;

        break;
      }

    }
  }
}
function randomFace() {
  //eyes changes here:
  var eyesPositions = myEyes.urls.length - 1;

  do {
    var xNumber = Math.floor((Math.random() * eyesPositions) + 0);
  }
  while (xNumber === myEyes.position);
  eyeSet.src = imageUrl + myEyes.urls[xNumber];
  myEyes.position = xNumber;
  //cejas changes here:
  var cejasPositions = myHead.urls.length - 1;

  do {
    var xNumber = Math.floor((Math.random() * cejasPositions) + 0);
  }
  while (xNumber === myHead.position);
  headSet.src = imageUrl + myHead.urls[xNumber];
  myHead.position = xNumber;


  //boca changes here:
  var bocaPositions = myBocas.urls.length - 1;

  do {
    var xNumber = Math.floor((Math.random() * bocaPositions) + 0);
  }
  while (xNumber === myBocas.position);
  bocaSet.src = imageUrl + myBocas.urls[xNumber];
  myBocas.position = xNumber;

  //dichos changes here:
  var dichoPositions = myDichos.urls.length - 1;

  do {
    var xNumber = Math.floor((Math.random() * dichoPositions) + 0);
  }
  while (xNumber === myDichos.position);
  dichoSet.src = imageUrl + myDichos.urls[xNumber];
  myDichos.position = xNumber;

}


function esSitio() {
  console.log("ES Sitio");
  //alert("espanhol");
  $("#intro").html(SiteContent.intro[0]);
  //Goals
  $("#goals1").html(SiteContent.goals1[0]);
  $("#goals2").html(SiteContent.goals2[0]);
  $("#goals3").html(SiteContent.goals3[0]);
  //Stickos
  $("#stickos-text").html(SiteContent.Stickos[0]);
  //iSpeakTICO
  $("#emoji-instr").html(SiteContent.iSpeakTICO[0]);
  //Contact
  $("#contact-text").html(SiteContent.contact[0]);
  //Emoji
  $("#emoji-caras").html(SiteContent.emojis[0]);
  $("#emoji-ojos").html(SiteContent.emojis[1]);
  $("#emoji-bocas").html(SiteContent.emojis[2]);
  $("#emoji-dichos").html(SiteContent.emojis[3]);
  //Shops
  $("#shop1").html(SiteContent.shop1[0]);
  $("#shop2").html(SiteContent.shop2[0]);
  $("#shop3").html(SiteContent.shop3[0]);
  $("#shop4").html(SiteContent.shop4[0]);
  $("#shop5").html(SiteContent.shop5[0]); //Cahuita
  //Menu
  $("#stk-getyours").html(SiteContent.navmenus[0]);
  $("#stk-contact").html(SiteContent.navmenus[1]);

  //Banner Text
  $("#banner1").html(SiteContent.bannertext[0]);
  $("#banner2").html(SiteContent.navmenus[0]);
  $("#banner3").html(SiteContent.bannertext[2]);

  //GreenTalk
  $("#greentalkheader").html(SiteContent.greentalkheader[0]);

  $("#greentalktitle1").html(SiteContent.greentalk1[0]);
  $("#greentalkdesc1").html(SiteContent.greentalk1[1]);
  $("#greentalktitle2").html(SiteContent.greentalk2[0]);
  $("#greentalkdesc2").html(SiteContent.greentalk2[1]);
  $("#greentalktitle3").html(SiteContent.greentalk3[0]);
  $("#greentalkdesc3").html(SiteContent.greentalk3[1]);

  //Titles
  $("#navGoals").html(SiteContent.sitetitles[0]);
  $("#g1").html(SiteContent.sitetitles[1]);
  $("#g2").html(SiteContent.sitetitles[2]);
  $("#g3").html(SiteContent.sitetitles[3]);
  $("#navShops").html(SiteContent.navmenus[0]);
  $("#navContact").html(SiteContent.navmenus[1]);

  //Form
  $("#formquestion").html(SiteContent.dichoForm[0]);
  $("#formlocation").html(SiteContent.dichoForm[2]);
  $("#formdicho").html(SiteContent.dichoForm[4]);

  //changes for the flag
  $("#espanholTico").addClass("selected");
  $("#englishGringo").removeClass("selected")
  $("#englishGringo").attr("src", "images/flag-usa-sm.png");
  $("#espanholTico").attr("src", "images/flag-tico-sm-on.png");

}

function enSite() {
  console.log("EN Site");
  //alert("english");
  $("#intro").html(SiteContent.intro[1]);
  //Goals
  $("#goals1").html(SiteContent.goals1[1]);
  $("#goals2").html(SiteContent.goals2[1]);
  $("#goals3").html(SiteContent.goals3[1]);
  //Stickos
  $("#stickos-text").html(SiteContent.Stickos[1]);
  //iSpeakTICO
  $("#emoji-instr").html(SiteContent.iSpeakTICO[1]);
  //Contact
  $("#contact-text").html(SiteContent.contact[1]);
  //Emoji
  $("#emoji-caras").html(SiteContent.emojis[4]);
  $("#emoji-ojos").html(SiteContent.emojis[5]);
  $("#emoji-bocas").html(SiteContent.emojis[6]);
  $("#emoji-dichos").html(SiteContent.emojis[7]);
  //Shops
  $("#shop1").html(SiteContent.shop1[1]);
  $("#shop2").html(SiteContent.shop2[1]);
  $("#shop3").html(SiteContent.shop3[1]);
  $("#shop4").html(SiteContent.shop4[1]);
  $("#shop5").html(SiteContent.shop5[1]);//Cahuita

  //Menu
  $("#stk-getyours").html(SiteContent.navmenus[2]);
  $("#stk-contact").html(SiteContent.navmenus[3]);

  //Banner Text
  $("#banner1").html(SiteContent.bannertext[1]);
  $("#banner2").html(SiteContent.navmenus[2]);
  $("#banner3").html(SiteContent.bannertext[3]);

  //GreenTalk
  $("#greentalkheader").html(SiteContent.greentalkheader[1]);

  $("#greentalktitle1").html(SiteContent.greentalk1[2]);
  $("#greentalkdesc1").html(SiteContent.greentalk1[3]);
  $("#greentalktitle2").html(SiteContent.greentalk2[2]);
  $("#greentalkdesc2").html(SiteContent.greentalk2[3]);
  $("#greentalktitle3").html(SiteContent.greentalk3[2]);
  $("#greentalkdesc3").html(SiteContent.greentalk3[3]);

  //Titles
  $("#navGoals").html(SiteContent.sitetitles[4]);
  $("#g1").html(SiteContent.sitetitles[5]);
  $("#g2").html(SiteContent.sitetitles[6]);
  $("#g3").html(SiteContent.sitetitles[7]);
  $("#navShops").html(SiteContent.navmenus[2]);
  $("#navContact").html(SiteContent.navmenus[3]);

  //Form
  $("#formquestion").html(SiteContent.dichoForm[1]);
  $("#formlocation").html(SiteContent.dichoForm[3]);
  $("#formdicho").html(SiteContent.dichoForm[5]);

  //changes for the flag
  $("#englishGringo").addClass("selected");
  $("#espanholTico").removeClass("selected");
  $("#englishGringo").attr("src", "images/flag-usa-sm-on.png");
  $("#espanholTico").attr("src", "images/flag-tico-sm.png");
}

function nextFace() {

  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      //alert ("about to change the "+ radios[i].value);
      if (radios[i].value === "eyes") { //eyes changes here:
        var movimiento = myEyes.position;
        var positionsArray = myEyes.urls.length - 1;
        //alert (movimiento +"-" +positionsArray);
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        if (movimiento <= positionsArray) {
          var sum = movimiento + 1;
          //alert("entro " + sum);
          if (sum <= positionsArray) {
            movimiento = sum;
            eyeSet.src = imageUrl + myEyes.urls[movimiento];
            myEyes.position = movimiento;
          } else {
            //alert("ultimo " + sum);
            eyeSet.src = imageUrl + myEyes.urls[0];
            myEyes.position = 0;
          }
        }
        break;
      } else if (radios[i].value === "boca") { //boca changes here:
        var movimiento = myBocas.position;
        var positionsArray = myBocas.urls.length - 1;
        //alert (movimiento +"-" +positionsArray);
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        if (movimiento <= positionsArray) {
          var sum = movimiento + 1;
          //alert("entro " + sum);
          if (sum <= positionsArray) {
            movimiento = sum;
            bocaSet.src = imageUrl + myBocas.urls[movimiento];
            myBocas.position = movimiento;
          } else {
            //alert("ultimo " + sum);
            bocaSet.src = imageUrl + myBocas.urls[0];
            myBocas.position = 0;
          }
        }
        break;
      } else if (radios[i].value === "dicho") { //boca changes here:
        var movimiento = myDichos.position;
        var positionsArray = myDichos.urls.length - 1;
        //alert (movimiento +"-" +positionsArray);
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        if (movimiento <= positionsArray) {
          var sum = movimiento + 1;
          //alert("entro " + sum);
          if (sum <= positionsArray) {
            movimiento = sum;
            dichoSet.src = imageUrl + myDichos.urls[movimiento];
            myDichos.position = movimiento;
          } else {
            //alert("ultimo " + sum);
            dichoSet.src = imageUrl + myDichos.urls[0];
            myDichos.position = 0;
          }
        }
        break;
      } else if (radios[i].value === "cejas") { //cejas changes here:
        var movimiento = myHead.position;
        var positionsArray = myHead.urls.length - 1;
        if (movimiento <= positionsArray) {
          var sum = movimiento + 1;
          //alert("entro " + sum);
          if (sum <= positionsArray) {
            movimiento = sum;
            headSet.src = imageUrl + myHead.urls[movimiento];
            myHead.position = movimiento;
          } else {
            //alert("ultimo " + sum);
            headSet.src = imageUrl + myHead.urls[0];
            myHead.position = 0;
          }
        }
        break;
      }

    }
  }

}
function prevFace() {

  for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      //alert ("about to change the "+ radios[i].value);
      if (radios[i].value === "eyes") { //eyes changes here:
        var movimiento = myEyes.position;
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
        if (movimiento >= 0) {
          //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
          //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
          //alert("movimiento " +movimiento);
          var rest = movimiento - 1;
          if (rest < 0) {
            movimiento = myEyes.urls.length - 1;
            eyeSet.src = imageUrl + myEyes.urls[movimiento];
            myEyes.position = movimiento;
          } else {
            eyeSet.src = imageUrl + myEyes.urls[movimiento - 1];
            myEyes.position = movimiento - 1;
          }
        }
        break;
      } else if (radios[i].value === "boca") { //eyes changes here:
        var movimiento = myBocas.position;
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
        if (movimiento >= 0) {
          //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
          //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
          //alert("movimiento " +movimiento);
          var rest = movimiento - 1;
          if (rest < 0) {
            movimiento = myBocas.urls.length - 1;
            bocaSet.src = imageUrl + myBocas.urls[movimiento];
            myBocas.position = movimiento;
          } else {
            bocaSet.src = imageUrl + myBocas.urls[movimiento - 1];
            myBocas.position = movimiento - 1;
          }
        }
        break;
      } else if (radios[i].value === "dicho") { //eyes changes here:
        var movimiento = myDichos.position;
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
        if (movimiento >= 0) {
          //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
          //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
          //alert("movimiento " +movimiento);
          var rest = movimiento - 1;
          if (rest < 0) {
            movimiento = myDichos.urls.length - 1;
            dichoSet.src = imageUrl + myDichos.urls[movimiento];
            myDichos.position = movimiento;
          } else {
            dichoSet.src = imageUrl + myDichos.urls[movimiento - 1];
            myDichos.position = movimiento - 1;
          }
        }
        break;
      } else if (radios[i].value === "cejas") { //eyes changes here:
        var movimiento = myHead.position;
        //alert("current position in json " + myEyes.position);
        //alert("current url "+ imageUrl + myEyes.urls[myEyes.position]);
        //alert("tamanho array "+ myEyes.urls.length); //muestra tamanho desde 1.. el array cuenta desde 0
        if (movimiento >= 0) {
          //alert("moviemento "+ movimiento + "new position in json " + myEyes.position);
          //alert("NEXT url "+ imageUrl + myEyes.urls[myEyes.position]);
          //alert("movimiento " +movimiento);
          var rest = movimiento - 1;
          if (rest < 0) {
            movimiento = myHead.urls.length - 1;
            headSet.src = imageUrl + myHead.urls[movimiento];
            myHead.position = movimiento;
          } else {
            headSet.src = imageUrl + myHead.urls[movimiento - 1];
            myHead.position = movimiento - 1;
          }
        }
        break;
      }

    }//radio check
  }

}

//https://www.w3schools.com/js/js_json_arrays.asp
//https://www.w3schools.com/js/tryit.asp?filename=tryjson_array_loop_in
//https://www.w3schools.com/js/js_json_objects.asp

function lado() {
  //alert(eyeSet.src);
  eyeSet.src = "https://image.ibb.co/iWWP58/eyes_7.png";
  headSet.src = "https://image.ibb.co/jsjNso/head_2.png";

  //trying to make it blink, improve with jQuery
  /*
  setTimeout(function(){ eyeSet.src = "https://image.ibb.co/hJEzNo/eyes_6.png"; }, 500);
  setInterval(function(){ eyeSet.src = "https://image.ibb.co/iWWP58/eyes_7.png"; }, 3000);
  */

}

function defaultFace() {
  //set the face to the default status
  //eyeSet.src = "https://image.ibb.co/gpQH8T/eyes_1.png";
  var eyesPos = myEyes.position;
  if (eyesPos >= 0) {
    eyeSet.src = imageUrl + myEyes.urls[eyesPos];
  } else {
    eyeSet.src = imageUrl + myEyes.urls[0];
  }

  var bocaPos = myBocas.position;
  if (bocaPos >= 0) {
    bocaSet.src = imageUrl + myBocas.urls[bocaPos];
  } else {
    bocaSet.src = imageUrl + myBocas.urls[0];
  }

  //headSet.src = "https://image.ibb.co/bH7KNo/head.png";//web
  headSet.src = "images/titi/zz-mono-cbz-cejas-arriba.png";//local
  //myVar = setInterval(normal, 1000);
}

function blink() {
  /*var eyesPos = myEyes.position -1;
  alert(myEyes.urls[eyesPos]);*/
  this.src = "https://image.ibb.co/hJEzNo/eyes_6.png";
}

function normal() {
  this.src = "https://image.ibb.co/gpQH8T/eyes_1.png";
}

function inlove() {
  this.src = "https://image.ibb.co/ivCav8/eyes_4.png";
}

/**
 * Back to Top Logic
 * 
 */
// When the user scrolls down 420px from the top of the document, show the button

function showTop(){
  document.getElementById("myBtnTop").style.display = "block";
}

function hideTop(){
  //to hide the back to top btn
  document.getElementById("myBtnTop").style.display = "none";
}

//https://www.w3schools.com/js/js_json_arrays.asp
//https://www.w3schools.com/js/tryit.asp?filename=tryjson_array_loop_in
//https://www.w3schools.com/js/js_json_objects.asp

/* Back to Top
vvvvvvvvvvvvvvvvv*/
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() { 
    if (document.body.scrollTop > 120 || document.documentElement.scrollTop > 120) {
        document.getElementById("myBtnTop").style.display = "block";
    } else {
        document.getElementById("myBtnTop").style.display = "none";
    }
}

/**
 * Modal JS
 */
// Open the Modal
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  if(document.getElementById("myModal") !== null){ //check if element exists
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
  }
  
}


/* Back to Top
Forms*/
$("#my-dicho").submit(function (e) {
  e.preventDefault();

  var $form = $(this);
  $.post($form.attr("action"), $form.serialize()).then(function () {
    alert("Thank you!");
  });
});

                  //https://youtu.be/ykrupgQgmkA?t=500

//by @Keno10cr .: